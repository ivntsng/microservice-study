import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

parameters = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='presentation_approval')
channel.queue_declare(queue='presentation_rejections')

def process_approval(ch, method, properties, body):
    message = json.loads(body)
    presenter_email = message['presenter_email']
    presenter_name = message['presenter_name']
    presentation_title = message['title']
    subject = 'Your presentation has been accepted'
    body = (
        f'{presenter_name}, we\'re happy to tell you that your presentation {presentation_title} has been accepted.'
    )
    send_mail(
        subject,
        body,
        'admin@conference.go',
        [presenter_email],
        fail_silently=False
    )

    print(f'Acceptance email sent to {presenter_email}')

def process_rejections(ch, method, properties, body):
    message = json.loads(body)
    presenter_email = message['presenter_email']
    presenter_name = message['presenter_name']
    presentation_title = message['title']
    subject = 'Your presentation has been rejected'
    body = (
        f'{presenter_name}, unfortunately your presentation {presentation_title} has been rejected.'
    )
    send_mail(
        subject,
        body,
        'admin@conference.go',
        [presenter_email],
        fail_silently=False
    )

    print(f'Rejection email sent to {presenter_email}')


channel.basic_consume(
    queue='presentation_approval',
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.basic_consume(
    queue='presentation_rejections',
    on_message_callback=process_rejections,
    auto_ack=True,
)

channel.start_consuming()
